using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientWeatherService
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Button1.Visibility = Visibility.Hidden;
            TextBox1.Visibility = Visibility.Hidden;
            TextBlock1.Visibility = Visibility.Hidden;
            string s = TextBox1.Text;
            TextBlock2.Text = Weather(s);
            TextBlock2.Visibility = Visibility.Visible;
            Button3.Visibility = Visibility.Visible;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Button1.Visibility = Visibility.Visible;
            TextBox1.Visibility = Visibility.Visible;
            TextBlock1.Visibility = Visibility.Visible;
            TextBlock2.Visibility = Visibility.Hidden;
            Button3.Visibility = Visibility.Hidden;
        }

        public string Weather(string message)
        {
            string url = "http://api.openweathermap.org/data/2.5/weather?q=" + message + "&units=metric&appid=3c5300eedc97c5170f480e109005712e";
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            string responce;

            using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
            {
                responce = streamReader.ReadToEnd();
            }

            WeatherResponse weatherResponse = JsonConvert.DeserializeObject<WeatherResponse>(responce);

            return weatherResponse.Name + " " + weatherResponse.Main.Temp.ToString() + " C";
        }

        public class TemperatureInfo
        {
            public float Temp { get; set; }
        }

        public class WeatherResponse
        {
            public TemperatureInfo Main { get; set; }
            public string Name { get; set; }
        }
    }
   
}
